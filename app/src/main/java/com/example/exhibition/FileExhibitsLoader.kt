package com.example.exhibition

import android.content.Context
import android.util.Log
import com.example.exhibition.objects.ExhibitBody
import com.example.exhibition.objects.ListObject
import com.google.gson.Gson
import com.google.gson.JsonParseException
import java.io.IOException
import java.io.InputStream


/**
 *  class which parse json from assets folder to List object
 *  @see ExhibitViewModel
 */
class FileExhibitsLoader(private val activity: Context) : ExhibitsLoader {
    override fun getExhibitList(): List<ExhibitBody> {
        val myJson = inputStreamToString(activity.assets.open("exhibit_json.json"))
        var list: List<ExhibitBody>?  = emptyList()
        try {
            val response = Gson().fromJson(myJson, ListObject::class.java)
            list = response.getEatList()
        } catch (e: JsonParseException) {
            Log.e("ERROR", e.toString())
        }
        return  list!!
    }

    private fun inputStreamToString(inputStream: InputStream): String? {
        return try {
            val bytes = ByteArray(inputStream.available())
            inputStream.read(bytes, 0, bytes.size)
            String(bytes)
        } catch (e: IOException) {
            null
        }

    }
}