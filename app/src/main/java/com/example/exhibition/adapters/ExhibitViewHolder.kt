package com.example.exhibition.adapters

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.chahinem.pageindicator.PageIndicator
import com.example.exhibition.objects.ExhibitBody
import kotlinx.android.synthetic.main.item_exhibit.view.*

class ExhibitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val imagesRecycler: RecyclerView by lazy { itemView.exhibit_images_recycler }

    private val exhibitTitle: TextView by lazy { itemView.exhibit_images_title }
    private val pageIndicator: PageIndicator by lazy { itemView.exhibit_page_indicator }

    fun bind(transaction: ExhibitBody, context: Context) {
        exhibitTitle.text = transaction.title
        val manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val mAdapter = ImagesAdapter(transaction.imgList)


        imagesRecycler.apply {
            layoutManager = manager
            adapter = mAdapter

        }
        pageIndicator.attachTo(imagesRecycler)
        val snapHelper = LinearSnapHelper()
        if (imagesRecycler.onFlingListener == null)
            snapHelper.attachToRecyclerView(imagesRecycler);

    }

}