package com.example.exhibition.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import com.example.exhibition.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_img.view.*


class ImagesAdapter(private val myDataset: List<String>) : RecyclerView.Adapter<ImagesAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_img, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(myDataset[position])
    }

    override fun getItemCount() = myDataset.size


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val exhibitImg: ImageView by lazy { itemView.exhibit_img }
        private val progress: ProgressBar by lazy { itemView.progress }

        fun bind(image: String) {

            val w  =itemView.width
            val h  =itemView.height

            Picasso.get()
                .load(image)
                .into(exhibitImg, object : Callback {
                    override fun onError(e: Exception?) {
                        progress.visibility = View.GONE
                    }
                    override fun onSuccess() {
                        progress.visibility = View.GONE

                    }
                })
        }
    }


}