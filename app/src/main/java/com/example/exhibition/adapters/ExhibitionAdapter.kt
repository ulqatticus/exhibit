package com.example.exhibition.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.exhibition.R
import com.example.exhibition.objects.ExhibitBody
import java.util.ArrayList

class ExhibitionAdapter(private var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var exhibitList = ArrayList<ExhibitBody>(0)


    override fun getItemCount(): Int = exhibitList.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ExhibitViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_exhibit,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val h = holder as ExhibitViewHolder
        h.bind(exhibitList[position], context)

    }

    fun addItem(list: List<ExhibitBody>?) {
        list?.let {
            exhibitList.clear()
            exhibitList.addAll(it)
            notifyDataSetChanged()
        }
    }
}