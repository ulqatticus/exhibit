package com.example.exhibition

import com.example.exhibition.objects.ExhibitBody

interface ExhibitsLoader {
    fun getExhibitList(): List<ExhibitBody>
}