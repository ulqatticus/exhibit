package com.example.exhibition

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.example.exhibition.objects.ExhibitBody

/**
 * ExhibitViewModel allow to keep data when device rotate
 * @see MainActivity
 * */
class ExhibitViewModel(application: Application) : AndroidViewModel(application) {
    private var liveData: MutableLiveData<List<ExhibitBody>>? = null

    fun getData(): LiveData<List<ExhibitBody>> {
        if (liveData == null) {
            liveData = MutableLiveData()
            setData()
        }
        return liveData as MutableLiveData<List<ExhibitBody>>
    }

    private fun setData() {
        val context: Context = getApplication()
        val list = FileExhibitsLoader(context).getExhibitList()
        liveData?.postValue(list)
    }
}