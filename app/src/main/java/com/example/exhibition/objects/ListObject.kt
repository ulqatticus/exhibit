package com.example.exhibition.objects

import com.google.gson.annotations.SerializedName


class ListObject {
    @SerializedName("list")
    private val eatList: List<ExhibitBody>? = null

    fun getEatList(): List<ExhibitBody>? {
        return eatList
    }
}