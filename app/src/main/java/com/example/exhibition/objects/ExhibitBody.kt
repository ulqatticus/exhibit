package com.example.exhibition.objects

import com.google.gson.annotations.SerializedName

data class ExhibitBody(
    @SerializedName("images")
    val imgList: List<String>,
    @SerializedName("title")
    val title: String
)