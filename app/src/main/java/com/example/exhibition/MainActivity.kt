package com.example.exhibition

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.exhibition.adapters.ExhibitionAdapter
import com.example.exhibition.objects.ExhibitBody
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val recyclerView: RecyclerView by lazy { recycler_view }
    private lateinit var maAdapter: ExhibitionAdapter
    private lateinit var model: ExhibitViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        model = ViewModelProviders.of(this).get(ExhibitViewModel::class.java)
        initRecyclerView()
        initLiveData()
    }

    private fun initLiveData() {
        val liveData = model.getData()
        liveData.observe(this, Observer<List<ExhibitBody>> {
            addData(it)
        })
    }


    private fun initRecyclerView() {
        val manager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        maAdapter = ExhibitionAdapter(this)
        recyclerView.apply {
            layoutManager = manager
            adapter = maAdapter
            setHasFixedSize(true)
        }
    }

    /**
     *  set data to recyclerView
     *  @param list - list with ExhibitBody which we got from ViewModel
     */
    private fun addData(list: List<ExhibitBody>?) {
        maAdapter.addItem(list)
    }


}
