# README #

This README would normally document whatever steps are necessary to get your application up and running.


### Technical task for Android ###

Develop an application which displays information about objects at the exhibitions. T
he application consists of one screen with a list, each row of which is intended for displaying one object.
Because the object can have several pictures, it is necessary to provide the possibility of changing them by using a horizontal scroll.
Display the object's name on top of each photo.
The project should consist of 3 separate modules:
* The Model module must contain only interfaces and the next classes:
* The Exhibit class, with two fields: title and images
* The ExhibitsLoader interface with one method: getExhibitList():List<Exhibit>
* The FileExhibitsLoader module, which contains only the implementation of the ExhibitsLoader interface by retrieving data from a JSON file 

* An Application that receives the data from the FileExhibitsLoader and performs its displaying according to the technical task.


### Evaluation Criteria ###
* Match with the task
* App architecture
* Code style and its readability
* Spent time


